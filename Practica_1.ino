#include "DHT.h"
#include <Servo.h>

//DHT11
#define DHTPIN 4 // Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

//ULTRASONICO
const int trigPin = 5;
const int echoPin = 18;
//define sound speed in cm/uS
#define SOUND_SPEED 0.034
#define CM_TO_INCH 0.393701
long duration;
float distanceCm;
float distanceInch;

// FOTORESISTOR
int sensorPin = 33; //define analog pin 5
int value = 0; 

//LEDS
int ledRojo = 27;
int ledVerde = 26;
int estadoLed = LOW;

//SERVO  
Servo myservo;
int angulo = 0;    // variable to store the servo position

//TIEMPO
int periodo = 30000;
unsigned long TiempoAhora = 0;

void setup() {
  Serial.begin(9600);

  dht.begin();

  //ULTRASONICO
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  
  //LEDS
  pinMode(ledRojo,OUTPUT);
  pinMode(ledVerde,OUTPUT);

  myservo.attach(13);  // attaches the servo on pin 13 to the servo object
}

void loop() {

  if(millis()> TiempoAhora + periodo){
    TiempoAhora = millis();

    // T E M P E R A T U R A
  
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  Serial.print(F("Humedad: "));
  Serial.print(h);
  Serial.println("%");
  Serial.print(F("Temperatura: "));
  Serial.print(t);
  Serial.println(F("°C "));

  //U L T R A S O N I C O
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculate the distance
  distanceCm = duration * SOUND_SPEED/2;
  // Prints the distance in the Serial Monitor
  Serial.print("Distancia (cm): ");
  Serial.println(distanceCm);

  // F O T O R E S I S T E N C I A
  value = analogRead(sensorPin); 
  Serial.print("Luminosidad: ");
  Serial.println(value, DEC);
  }
  
  //L E D S
  if(Serial.available()){
    char dato = Serial.read();
    
    if(dato == 't'){
      estadoLed = digitalRead(ledRojo); 
      digitalWrite(ledRojo,!estadoLed);
      //Serial.println("Rojo");
    }
    else if(dato == 'y'){
      estadoLed = digitalRead(ledVerde); 
      digitalWrite(ledVerde,!estadoLed);
      //Serial.println("Verde");
    }
  }  

  //S E R V O
  if(Serial.available()){
    int angulo= Serial.read();
    
    if(angulo>=0 && angulo<=180){
     myservo.write(angulo);
    } 
  }
}
